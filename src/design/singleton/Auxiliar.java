package design.singleton;

public class Auxiliar {
	Contador c = 
			Contador.getInstance();
	
	public void emiteFicha() {
		int v = c.proximo(); 
		//retorna um inteiro incrementado
		System.out.println(
				"Prox num:" + v
		);
	}

}
