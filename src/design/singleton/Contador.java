package design.singleton;
public class Contador {
	int cnt = 0;
	static Contador instance = null;
	private Contador() {
		System.out.println("Fui "
				+ "instanciado");
	}
	public static Contador 
	   getInstance() {
		 if (instance == null)
			instance = new Contador();
		 return instance;
	}
	public int proximo() {
		return ++cnt;
	}

}
