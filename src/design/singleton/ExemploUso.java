package design.singleton;

public class ExemploUso {
	public static void main(String[] args) {		
		Auxiliar aux = 
				new Auxiliar();
		for (int i=0;i<20;i++) 
			{
			aux.emiteFicha();
			aux.emiteFicha();
			}
		Auxiliar2 aux2 = 
				new Auxiliar2();
		for (int i=0;i<20;i++) {
			aux2.emiteFicha();
			aux2.emiteFicha();
		}
	}
}
