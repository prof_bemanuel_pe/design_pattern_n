package design.factory;

import java.util.Random;

import design.factory.forma.Redondo;
import design.factory.forma.Forma;
import design.factory.forma.Quadrado;

public class Fabrica {
	private Random rand = new Random();

	public enum Tipo {
		QUADRADO, CIRCULO, RETANGULO;
	}

	public Forma criaForma(Tipo t) {
		Forma res = null;
		switch (t) {
		case CIRCULO:
			res = new Redondo(rand.nextInt());
			break;
		case QUADRADO:
			res = new Quadrado(rand.nextInt());
			break;
		case RETANGULO:
			res = new Quadrado(rand.nextInt(), rand.nextInt());
			break;
		}

		/*
		 * if (t == Tipo.CIRCULO) res = new Redondo(rand.nextInt()); if (t ==
		 * Tipo.QUADRADO) res = new Quadrado(rand.nextInt()); if (t == Tipo.RETANGULO)
		 * res = new Quadrado(rand.nextInt(), rand.nextInt());
		 */
		return res;
	}

}
