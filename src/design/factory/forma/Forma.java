package design.factory.forma;

public interface Forma {

	void desenha();
	void area();

}