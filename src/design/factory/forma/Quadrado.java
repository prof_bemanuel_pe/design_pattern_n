package design.factory.forma;

public class Quadrado implements Forma {
	int base = 0;
	int altura = 0;

	public Quadrado(int b) {
		base = b;
		altura = b;
	}

	public Quadrado(int b, int h) {
		base = b;
		altura = h;
	}

	public void desenha() {
		String forma = (base != altura ? "retangulo" : "quadrado");
		System.out.println("Desenha: " + forma);
	}

	public void area() {
		System.out.println("Area:" + base * altura);
	}

}
