package design.factory.forma;

public class Redondo implements Forma {
	int r = 0;
	public Redondo(int raio) {
		r = raio;
	}
	@Override
	public void desenha() {
		System.out.println("Desenha: "
				+ "Circulo");
	}

	@Override
	public void area() {
		System.out.println("Area: "
				+ r*r);
	}
}
