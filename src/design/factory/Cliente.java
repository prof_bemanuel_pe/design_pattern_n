package design.factory;

import design.factory.forma.Forma;

public class Cliente {

	public static void main(String[] args){
		Fabrica f = new Fabrica();
		Forma c = f.criaForma(Fabrica.Tipo.CIRCULO);
		Forma q = f.criaForma(Fabrica.Tipo.QUADRADO);
		Forma ret = f.criaForma(Fabrica.Tipo.RETANGULO);
		
		c.desenha();
		q.desenha();
		ret.desenha();
		
		c.area();
		q.area();
		ret.area();
	}

}
